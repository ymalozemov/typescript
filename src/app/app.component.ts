import {Component, OnInit} from '@angular/core';

export interface LabeledValue {
  label: string;
  name: string;
}

export interface Point {
  readonly x: number;
  readonly y: number;
  color?: string;
}

export interface SquareConfig {
  color?: string;
  width?: number;
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'ConventionalCommits';

  ngOnInit(): void {
    const list: number[] = [1, 2, 3];
    const list2: Array<number> = [1, 2, 3];
    console.log(list);
    console.log(list2);

    enum Color {
      Red = 3,
      Green = 6,
      Blue
    }

    const green: Color = Color.Green;
    const red: Color = Color.Red;
    const blue: Color = Color.Blue;
    console.log(green, red, blue);
    const label: LabeledValue = {
      label: 'label',
      name: 'newName'
    };
    const point: Point = {x: 10, y: 20};
    console.log(point.x);
    console.log(label);
    console.log(point.color);

    // point.x = 3
    function createSquare(config: SquareConfig): { color: string; area: number } {
      return {
        color: config.color || 'red',
        area: config.width ? config.width * config.width : 20,
      };
    }

    const mySquare = createSquare({color: 'red', width: 100});
    console.log(mySquare);

    type SearchFunc = (source: string, subString: string) => boolean;

    let mySearch: SearchFunc;

    mySearch = (source: string, subString: string) => {
      const result = source.search(subString);
      return result > -1;
    };
    mySearch('1', '2');
  }

}
